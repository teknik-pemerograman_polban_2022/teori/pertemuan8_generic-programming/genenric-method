public class App {
    public static void main(String[] args) throws Exception {
        Display monitor = new Display();
        String[] arrOfString = { "Aku", "mahasiswa", "POLBAN" };
        Integer[] arrOfInteger = { 5, 4, 3, 2, 1 };
        Character[] arrOfChar = { 'A', 'B', 'C', 'D' };

        System.out.println("====Proses print segala tipe array dan elemen terbesar dari masing masing array====\n");
        System.out.println("Array bertipe string :");
        monitor.printArray(arrOfString);
        monitor.printMaxArray(arrOfString);
        System.out.println();
        System.out.println("Array bertipe integer :");
        monitor.printArray(arrOfInteger);
        monitor.printMaxArray(arrOfInteger);
        System.out.println();
        System.out.println("Array bertipe karakter :");
        monitor.printArray(arrOfChar);
        monitor.printMaxArray(arrOfChar);
        System.out.println();
    }
}
