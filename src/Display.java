public class Display {
    public static <E> void printArray(E[] inputArray) {
        // Display array elements
        for (E element : inputArray) {
            System.out.printf("%s ", element);
        }
        System.out.println();
    }

    public static <T extends Comparable<T>> void printMaxArray(T[] inputArray) {
        T Max = inputArray[0];
        for (int i = 1; i < inputArray.length; i++) {
            if (inputArray[i].compareTo(Max) > 0) {
                Max = inputArray[i];
            }
        }
        System.out.printf("(%s)\n", Max);
    }
}
